var $sidebarAndWrapper = $("#sidebar,#main");

$("#sidebarToggle").click(function () {
	$sidebarAndWrapper.toggleClass("hide-sidebar");
	$("#sidebarToggle i").toggleClass("fa-times");

	if ($sidebarAndWrapper.hasClass("hide-sidebar")) $("#navIcon").removeClass("fa-bars").addClass("fa-times");
	else $("#navIcon").removeClass("fa-times").addClass("fa-bars");
});
